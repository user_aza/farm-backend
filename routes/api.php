<?php

use App\Http\Controllers\CorralController;
use Illuminate\Support\Facades\Route;

Route::get('/corrals', [CorralController::class, 'index']);
Route::put('/corrals/move-sheep', [CorralController::class, 'moveSheep']);
Route::post('/corrals/add-sheep', [CorralController::class, 'addSheep']);
Route::put('/corrals/update-sheep', [CorralController::class, 'updateSheep']);
Route::delete('/corrals/delete-sheep', [CorralController::class, 'deleteSheep']);
Route::post('/corrals/execute-command', [CorralController::class, 'executeCommand']);
Route::get('/corrals/get-report', [CorralController::class, 'report']);
