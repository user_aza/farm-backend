<?php

use Illuminate\Database\Seeder;

class CorralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Corral::insert([
            ['number' => 1],
            ['number' => 2],
            ['number' => 3],
            ['number' => 4],
        ]);
    }
}
