
## Instructions

#### run following commands:
- git clone https://gitlab.com/user_aza/farm-backend.git
- cd farm-backend
- composer install
- echo "CREATE DATABASE farm DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci" | mysql -uroot -p
- cp .env.example .env
- php artisan key:generate 
- php artisan migrate
- php artisan db:seed
- php artisan serve


### Api route list

- GET|HEAD | api/corrals
- POST     | api/corrals/add-sheep      
- DELETE   | api/corrals/delete-sheep
- POST     | api/corrals/execute-command
- GET|HEAD | api/corrals/get-report 
- PUT      | api/corrals/move-sheep  
- PUT      | api/corrals/update-sheep 


### Console commands
- php artisan  corral:add-sheep 
- php artisan  corral:delete-sheep 
- php artisan  corral:move-sheep 
