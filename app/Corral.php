<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Corral extends Model
{
    protected $table = 'corrals';
    protected $fillable = [
        'id',
        'number'
    ];

    public function sheeps() {
        return $this->hasMany(Sheep::class);
    }

}
