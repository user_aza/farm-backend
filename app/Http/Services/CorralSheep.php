<?php


namespace App\Http\Services;

use App\Corral;
use App\Sheep;
use DateTime;

class CorralSheep
{
    public function randomGiveOut()
    {
        $sheep = Sheep::query()->get();
        if (!$sheep->count()) {
            $corralIds = Corral::query()->pluck('id');
            $randomDevice = $this->randomDivision(10, 4);
            $datetime = new DateTime();
            foreach ($corralIds as $key => $corralId) {
                for ($i = 0; $i < $randomDevice[$key + 1]; $i++) {
                    Sheep::query()->insert([
                        'corral_id' => $corralId,
                        'created_at' => $datetime->format('Y-m-d ').'00:00:00',
                        'updated_at' => $datetime->format('Y-m-d ').'00:00:00'
                    ]);
                }
            }
        }
    }

    public function getDay()
    {
        try {
            $firstDate = new DateTime(Sheep::query()->min('created_at'));
            $lastDate = new DateTime(Sheep::query()->max('created_at'));
            $interval = $firstDate->diff($lastDate);
            return $interval->d;
        } catch (\Exception $e) {
            info($e->getMessage());
        }
    }

    private function randomDivision($x, $chast)
    {
        for ($i = 1; $i < $chast; $i++) {
            $temp[$i] = $x - rand(1, $x);
            $x = $x - $temp[$i];
        }
        $temp[$chast] = $x;
        return $temp;
    }

    public function moveSheepBetweenCorrals(array $data)
    {
        $sheep = Sheep::query()->where('corral_id', $data['from'])->first();
        if ($sheep) {
            $sheep->corral_id = $data['to'];
            $sheep->save();
            return true;
        }
        return false;
    }

    public function addOneSheepToEveryCorrals(array $data)
    {
        foreach ($data['corralIds'] as $corralId) {
            $this->addSheepToCorral([
                $data['date'],
                $corralId,
            ]);
        }
    }

    public function deleteOneSheepToEveryCorrals(array $data)
    {
        foreach ($data['corralIds'] as $corralId) {
            $this->deleteSheepCorral([
                $data['date'],
                $corralId,
            ]);
        }
    }

    public function addSheepToCorral($data)
    {
        try {
            $datetime = new DateTime($data[0]);
            Sheep::query()->insert([
                'corral_id' => $data[1],
                'created_at' => $datetime->format('Y-m-d ').'00:00:00',
                'updated_at' => $datetime->format('Y-m-d ').'00:00:00'
            ]);
            return true;
        } catch (\Exception $e) {
            info($e->getMessage());
            return false;
        }
    }

    public function deleteSheepCorral($data)
    {
        try {
            $datetime = new DateTime($data[0]);
            $sheep = Sheep::query()->where('corral_id', $data[1])->first();
            $sheep->deleted_at = $datetime->format('Y-m-d ').'00:00:00';
            $sheep->save();
            return true;
        } catch (\Exception $e) {
            info($e->getMessage());
            return false;
        }
    }

    public function moveSheep(array $data)
    {
        try {
            $datetime = new DateTime($data[0]);
            $sheep = Sheep::query()->where('corral_id', $data[1])->first();
            if ($sheep) {
                $sheep->corral_id = $data[2];
                $sheep->updated_at =  $datetime->format('Y-m-d ').'00:00:00';
                $sheep->save();
                return true;
            }
            return false;
        } catch (\Exception $e) {
            info($e->getMessage());
            return false;
        }

    }
}
