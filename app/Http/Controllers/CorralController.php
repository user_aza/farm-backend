<?php

namespace App\Http\Controllers;

use App\Corral;
use App\Http\Resources\CorralResource;
use App\Http\Services\CorralSheep;
use App\Sheep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class CorralController extends Controller
{

    /**
     * @var CorralSheep
     */
    private $corralSheepService;

    public function __construct(CorralSheep $corralSheepService)
    {
        $this->corralSheepService = $corralSheepService;
    }

    public function index()
    {
        $this->corralSheepService->randomGiveOut();
        return CorralResource::collection(Corral::with('sheeps')->get())
            ->additional(['day' => $this->corralSheepService->getDay()]);
    }

    public function moveSheep(Request $request)
    {
        $this->corralSheepService->moveSheepBetweenCorrals($request->all());
        return CorralResource::collection(Corral::with('sheeps')->get());
    }


    public function addSheep(Request $request)
    {
        $this->corralSheepService->addOneSheepToEveryCorrals($request->all());
        return CorralResource::collection(Corral::with('sheeps')->get());
    }


    public function updateSheep(Request $request)
    {
        DB::table('sheeps')->truncate();
        $this->corralSheepService->randomGiveOut();
        return CorralResource::collection(Corral::with('sheeps')->get())
            ->additional(['day' => $this->corralSheepService->getDay()]);
    }


    public function deleteSheep(Request $request)
    {
        $this->corralSheepService->deleteOneSheepToEveryCorrals($request->all());
        return CorralResource::collection(Corral::with('sheeps')->get());
    }


    public function executeCommand(Request $request)
    {
        Artisan::call('corral:' . $request->get('commandName'), [
            'data' => $request->get('data'),
        ]);
        return CorralResource::collection(Corral::with('sheeps')->get());
    }

    public function report()
    {
        $maxSheep = Sheep::groupBy('corral_id')->selectRaw('count(*) as total, corral_id')->orderBy('total', 'desc')->first();
        $maxSheep = "загон-{$maxSheep->corral_id} ({$maxSheep->total})";

        $minSheep = Sheep::groupBy('corral_id')->selectRaw('count(*) as total, corral_id')->orderBy('total', 'asc')->first();
        $minSheep = "загон-{$minSheep->corral_id} ({$minSheep->total})";

        return response()->json([
            'Общее количество овечек' => Sheep::withTrashed()->count(),
            'Количество живых овечек' => Sheep::count(),
            'Количество убитых овечек' => Sheep::onlyTrashed()->count(),
            'Номер самого населённого загона' => $maxSheep,
            'Номер самого менее населённого загона' => $minSheep
        ]);
    }
}
