<?php

namespace App\Console\Commands;

use App\Http\Services\CorralSheep;
use Illuminate\Console\Command;

class MoveSheep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'corral:move-sheep {data*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move sheep into corral. Укажите корректную дату в формате yy-mm-dd, существующий id загона и id загона для перемещения';

    /**
     * @var CorralSheep
     */
    private $corralSheepService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CorralSheep $corralSheepService)
    {
        parent::__construct();
        $this->corralSheepService = $corralSheepService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (count($this->argument('data')) < 3) {
            $this->error('Укажите id загона, id загона для перемещения и дату');
        } else {
            $res = $this->corralSheepService->moveSheep($this->argument('data'));
            if ($res) {
                $this->info('Успешно выполнено!');
            } else {
                $this->warn('Укажите корректную дату в формате yy-mm-dd, существующий id загона и id загона для перемещения');
            }
        }
    }
}
