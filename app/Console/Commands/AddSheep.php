<?php

namespace App\Console\Commands;

use App\Http\Services\CorralSheep;
use Illuminate\Console\Command;

class AddSheep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'corral:add-sheep {data*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add sheep into corral. Укажите корректную дату в формате yy-mm-dd и существующий id загона';
    /**
     * @var CorralSheep
     */
    private $corralSheepService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CorralSheep $corralSheepService)
    {
        parent::__construct();
        $this->corralSheepService = $corralSheepService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (count($this->argument('data')) < 2) {
            $this->error('Укажите ид загона и дату');
        } else {
            $res = $this->corralSheepService->addSheepToCorral($this->argument('data'));
            if ($res) {
                $this->info('Успешно выполнено!');
            } else {
                $this->warn('Не добавлено! Укажите корректную дату в формате yy-mm-dd и существующий id загона');
            }
        }
    }
}
