<?php

namespace App\Console\Commands;

use App\Http\Services\CorralSheep;
use Illuminate\Console\Command;

class DeleteSheep extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'corral:delete-sheep {data*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete sheep corral. Укажите корректную дату в формате yy-mm-dd и существующий id загона';

    /**
     * @var CorralSheep
     */
    private $corralSheepService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CorralSheep $corralSheepService)
    {
        parent::__construct();
        $this->corralSheepService = $corralSheepService;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (count($this->argument('data')) < 2){
            $this->error('Укажите id загона и дату');
        } else {
            $res = $this->corralSheepService->deleteSheepCorral($this->argument('data'));
            if ($res){
                $this->info('Успешно выполнено!');
            } else {
                $this->warn('Не удалено! Укажите корректную дату в формате yy-mm-dd и существующий id загона');
            }
        }
    }
}
