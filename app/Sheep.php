<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sheep extends Model
{
    use SoftDeletes;

    protected $table = 'sheeps';
    protected $fillable = [
        'id',
        'corral_id'
    ];
}
